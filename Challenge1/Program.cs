﻿namespace Challenge1;
class Program
{
    static void Main(string[] args)
    {
        Console.Write("Введите целое число: ");
        string strFromConsole = Console.ReadLine()!;
        if (int.TryParse(strFromConsole, out int number))
        {
            if (number % 2 == 0)
            {
                Console.WriteLine($"Число {number} является четным");
            }
            else
            {
                Console.WriteLine($"Число {number} является нечетным");
            }
        }
        else
        {
            Console.WriteLine("Необходимо ввести целое число");
        }
        Console.ReadKey();
    }
}