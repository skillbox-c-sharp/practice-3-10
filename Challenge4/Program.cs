﻿namespace Challenge4;
class Program
{
    static void Main(string[] args)
    {
        int minValue = int.MaxValue;
        Console.Write("Введите длину последовательности последовательности: ");
        string strFromConsole = Console.ReadLine()!;
        if (int.TryParse(strFromConsole, out int count))
        {
            for (int i = 0; i < count; i++)
            {
                Console.Write($"Введите целое число #{i + 1}: ");
                string numberFromConsole = Console.ReadLine()!;
                if (int.TryParse(numberFromConsole, out int number))
                {
                    if (number < minValue) minValue = number;
                }
                else
                {
                    Console.WriteLine("Необходимо ввести целое число");
                    i--;
                }
            }
        }
        else
        {
            Console.WriteLine("Необходимо ввести целое число");
        }
        Console.WriteLine($"Минимальное число в последовательности: {minValue}");
        Console.ReadKey();
    }
}

