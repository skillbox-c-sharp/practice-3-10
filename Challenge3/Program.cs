﻿namespace Challenge3;
class Program
{
    static void Main(string[] args)
    {
        bool isPrime = true;
        Console.Write("Введите целое число: ");
        string strFromConsole = Console.ReadLine()!;
        if (int.TryParse(strFromConsole, out int number))
        {
            int i = 2;
            while (i < number)
            {
                if(number % i == 0)
                {
                    isPrime = false;
                    break;
                }
                i++;
            }
        }
        else
        {
            Console.WriteLine("Необходимо ввести целое число");
        }
        if(isPrime) Console.WriteLine($"Число {number} является простым");
        else Console.WriteLine($"Число {number} не является простым");

        Console.ReadKey();
    }
}

