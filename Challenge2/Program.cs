﻿namespace Challenge2;
class Program
{
    static void Main(string[] args)
    {
        int weight = 0;
        Console.Write("Введите количетсво карт у Вас на руке: ");
        string strFromConsole = Console.ReadLine()!;
        if (int.TryParse(strFromConsole, out int count))
        {
            for (int i = 0; i < count; i++)
            {
                Console.Write($"Введите карту #{i + 1}: ");
                string cardFromConsole = Console.ReadLine()!;
                if (int.TryParse(cardFromConsole, out int cardNumber))
                {
                    if(cardNumber >= 2 && cardNumber <= 10)
                    {
                        weight += cardNumber;
                        Console.WriteLine($"Текущая сумма: {weight}");
                    }
                    else
                    {
                        Console.WriteLine("Такой карты не существует!");
                        i--;
                    }
                }
                else
                {
                    switch (cardFromConsole)
                    {
                        case "J":
                            weight += 10;
                            Console.WriteLine($"Текущая сумма: {weight}");
                            break;
                        case "Q":
                            weight += 10;
                            Console.WriteLine($"Текущая сумма: {weight}");
                            break;
                        case "K":
                            weight += 10;
                            Console.WriteLine($"Текущая сумма: {weight}");
                            break;
                        case "A":
                            weight += 11;
                            Console.WriteLine($"Текущая сумма: {weight}");
                            break;
                        default:
                            Console.WriteLine("Такой карты не существует!");
                            i--;
                            break;
                    }
                }
            }
            Console.WriteLine($"Итоговая сумма: {weight}");

        }
        else
        {
            Console.WriteLine("Необходимо ввести целое число");
        }
        Console.ReadKey();
    }
}

