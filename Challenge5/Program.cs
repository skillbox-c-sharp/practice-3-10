﻿namespace Challenge5;
class Program
{
    static void Main(string[] args)
    {
        Random rand = new Random();
        Console.Write("Введите максимальное число для генерации: ");
        string strFromConsole = Console.ReadLine()!;
        if (int.TryParse(strFromConsole, out int maxNumber))
        {
            int randomNumber = rand.Next(0, maxNumber + 1);
            Console.WriteLine(randomNumber);
            int i = 0;
            while (true)
            {
                Console.Write("Введите предполагаемое число: ");
                string numberFromConsole = Console.ReadLine()!;
                if(numberFromConsole == "")
                {
                    Console.WriteLine($"Загаданное число: {randomNumber}");

                    break;
                }
                else if (int.TryParse(numberFromConsole, out int userNumber))
                {
                    i++;
                    if (userNumber < randomNumber)
                    {
                        Console.WriteLine("Введеное число меньше загаданного. Попробуйте еще раз");
                    }
                    else if (userNumber > randomNumber)
                    {
                        Console.WriteLine("Введеное число больше загаданного. Попробуйте еще раз");

                    }
                    else
                    {
                        Console.WriteLine($"Число угадано! Попыток: {i}");
                        break;
                    }
                }
                else
                {
                    Console.WriteLine("Необходимо ввести целое число");
                }
            }
        }
        else
        {
            Console.WriteLine("Необходимо ввести целое число");
        }
        Console.ReadKey();
    }
}

